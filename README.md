#Jyst - eslint rules for writeing javascript 

This repo contains eslint rules used at jyst creative for writing nodejs javascript applications using ES6.
This should not be used for writing React applications.

Rules are based on AirBnB's eslint base configuration. 

##Installation

Yarn: `yarn add --dev eslint babel-eslint eslint-plugin-import eslint-plugin-no-unused-vars-rest eslint-config-airbnb-base`

NPM: `npm i --save-dev eslint babel-eslint eslint-plugin-import eslint-plugin-no-unused-vars-rest eslint-config-airbnb-base`

 - Copy content of `.eslintrc` and `.editorconfig` into project directory.
 - Find plugin for eslint and editorconfig for your favorite text editor
